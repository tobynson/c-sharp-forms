﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Se declara la palabra res y se realiza la funcion de la suma.
            double res = double.Parse(textBox1.Text) + double.Parse(textBox2.Text);
            //Aqui se muestra los valores que introducimos para realizar la suma.
            textBox3.Text = textBox1.Text + " + " + textBox2.Text;
            //Aqui se indica que el resultado se mostrara en el textbox4 que es el del resultado.
             textBox4.Text = res.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Sirve para borrar los datos introducidos en los campos.
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();

        }
    }
}
