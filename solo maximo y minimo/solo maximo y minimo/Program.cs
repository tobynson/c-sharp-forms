﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class test_array
    {
        //campo de datos
        //arreglo tipo float para que el promedio sea en decimales
        public float[] notas;
        public string []nombres;
        //tamaño del vector
        int n;
        float max, min;
       
        public void inicializar()
        {
            //leer tamaño del vector
            Console.Write("cuantos notas desea ingresar: ");
            n = int.Parse(Console.ReadLine());
            //inicializar arreglos
            notas = new float[n];
            nombres = new string[n];
            //recorrer cada arreglo y leer datos
            for (int i = 0; i < notas.Length; i++)
            {
                Console.Write("ingrese nombre {0}: ", i + 1);
                nombres[i] = Console.ReadLine();  
                Console.Write("ingrese nota {0}: ", i + 1 );
                notas[i]= float.Parse(Console.ReadLine());               
            }
            Console.WriteLine();
            //Console.Clear();
                        
        }
        public void mayor_menor()
        {
            //asignar el valor de la posicion 0 del array notas a max y min
            max = notas[0];
            min = notas[0];
           //recorrer el array notas
            for (int i = 0; i < notas.Length; i++)
            {
                //si la posicion i del array notas es mayor a max
                //se le asigna ese valor a max
                if (notas[i] > max)
                    max = notas[i];
                   
                //else
                  if (notas[i] <min)
                    min = notas[i];                  
            }

            Console.Write("la nota maxima es: " + max +"\n");
            Console.Write("la nota minima es: " + min + "\n");           
        }

        public void mayor_y_menor()
        {
            //recorrer el array notas
            for (int i = 0; i < notas.Length; i++)
            {
                //si el contenido de la posicion i es igual a max
                if (notas[i] == max)
                {
                    Console.WriteLine("{0} tiene la nota maxima de : {1} ", nombres[i], max);
                }
            }

            for (int i = 0; i < notas.Length; i++)
            {
                if (notas[i] == min)
                {
                    Console.WriteLine("{0} tiene la nota minima de : {1} ", nombres[i], min);
                }
            }
        }

        public void promedio()
        {
            //acumulador s de tipo float para obtener un decimal
           float s = 0;
           float prom;
            for (int i = 0; i < notas.Length; i++)
            {
                s = s + notas[i];               
            }
            prom = s / notas.Length;
            Console.WriteLine("el promedio de notas es: " +prom);
        }
        static void Main(string[] args)
        {
            test_array obj = new test_array();
            obj.inicializar();
            obj.mayor_menor();
            obj.mayor_y_menor();
            obj.promedio();
            Console.ReadKey();
        }
    }
}