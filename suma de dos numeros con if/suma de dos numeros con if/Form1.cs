﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public int numero1, numero2;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("el nombre no puede estar vacio ");
            }
            else if (textBox2.Text.Length == 0 || textBox3.Text.Length == 0)
            {
                MessageBox.Show("numero1 y numero2 no pueden estar vacios");

            }
            else
            {

                numero1 = int.Parse(textBox2.Text);
                numero2 = int.Parse(textBox3.Text);
                int res = numero1 + numero2;

                MessageBox.Show(Convert.ToString(res));

            }
        }
    }
}
