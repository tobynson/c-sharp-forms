﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class nueva_clase
    {
        //arreglos
        private string[] nombres;
        private int[] edad;
        //tamaño de los arreglos
        int n;
        public void inicializar()
        {
            //leer tamalo del arreglo
            Console.WriteLine("cuantos datos desea procesar");
            n = int.Parse(Console.ReadLine());
            //inicializar vectores
            nombres = new string[n];
            edad = new int[n];
            //leer elementos del array puede ser nombres o edad
            //por ser inidimensionales del mismo tamaño
            for (int f = 0; f < nombres.Length; f++)
            {
                Console.WriteLine("ingrese nombre: ");
                nombres[f] = Console.ReadLine();
                Console.WriteLine("ingrese edad: ");
                edad[f] = int.Parse(Console.ReadLine());
            }
            Console.Clear();
       }
        public void imprimir()
        {
            for (int f = 0; f < nombres.Length; f++)
            {
                Console.Write("\t nombres{0} \t\n edad{1}", nombres[f], edad[f]);
            }
        }
    }
}
