﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Al declararlo en este lugar las variables podrán ser llamadas desde cualquier parte del código
        public int numero1, numero2;

        private void button1_Click(object sender, EventArgs e)
        {
            numero1 = int.Parse(textBox1.Text);
            numero2 = int.Parse(textBox2.Text);
                    
            MessageBox.Show(Convert.ToString(numero1 + numero2));

            //Lo que hice aquí fue convertir los datos ingresados en datos numéricos, 
            //para posteriormente almacenarlos en las variables que declare.
            //Después muestro el resultado de la suma en un MessageBox previamente convertido a "String" (Cadena de caracteres).

        }

        private void button3_Click(object sender, EventArgs e)
        {
            numero1 = int.Parse(textBox1.Text);

            numero2 = int.Parse(textBox2.Text);

            MessageBox.Show(Convert.ToString(numero1 * numero2));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            numero1 = int.Parse(textBox1.Text);

            numero2 = int.Parse(textBox2.Text);

            MessageBox.Show(Convert.ToString(numero1 - numero2));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            numero1 = int.Parse(textBox1.Text);

            numero2 = int.Parse(textBox2.Text);

             MessageBox.Show(Convert.ToString(numero1 / numero2));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

       

        
    }
}
