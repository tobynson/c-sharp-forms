﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n_horas;
            Console.Write("ingrese numero de horas trabajadas: ");
            n_horas = int.Parse(Console.ReadLine());

            if (n_horas <= 40)
            {
                int ganancia = 16 * n_horas;
                Console.Write("su sueldo es "+ ganancia);
            }
            else
            {
                int t = n_horas - 40;
                int ganancia = 16 * n_horas; 
                int sobretiempo = t*20;
                int total = ganancia+sobretiempo;
                Console.WriteLine(" \n su sueldo es {0}\n su sobretiempo es {1}\n el total es {2} ", ganancia, sobretiempo, total);
                
            }
            Console.ReadLine();
        }
        
    }
}
