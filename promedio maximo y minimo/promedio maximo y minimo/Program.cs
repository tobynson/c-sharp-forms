﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PruebaVector2
{
    class PruebaVector2
    {
        private string[] alumnos;
        private float[] alturas;
        private float promedio;
        //tamaño del arreglo
        int n;

        public void Cargar()
        {
            Console.Write("cuantos datos desea cargar: ");
            n = int.Parse(Console.ReadLine());
            alumnos = new string[n];
            alturas = new float[n];
            

            for (int f = 0; f < alturas.Length; f++)
            {
                Console.Write("Ingrese nombre del alumno:");
                alumnos[f] = Console.ReadLine();
                Console.Write("Ingrese la altura de la persona:");
                alturas[f] = float.Parse(Console.ReadLine());
                
            }
        }

        public void CalcularPromedio()
        {
            //suma de elementos del array
            float suma;
            suma = 0;
            for (int f = 0; f < alturas.Length; f++)
            {
                suma = suma + alturas[f];
            }
            //promedio de elementos del array
            promedio = suma / alturas.Length;
            Console.WriteLine("Promedio de alturas:" + promedio);
        }

        public void MayoresMenores()
        {
            int may, men;
            may = 0;
            men = 0;
            for (int f = 0; f < alturas.Length; f++)
            {
                if (alturas[f] > promedio)
                {
                    may++;
                }
                else
                {
                    if (alturas[f] < promedio)
                    {
                        men++;
                    }
                }
            }

            Console.WriteLine("Cantidad de personas mayores al promedio:" + may);
            Console.WriteLine("Cantidad de personas menores al promedio:" + men);

        }

        public void may()
        {
            float max = alturas[0];
            float min = alturas[0];

            for (int f = 0; f < alturas.Length; f++)
            {
                if (alturas[f] > max)
                    max = alturas[f];

                if (alturas[f] < min)
                    min = alturas[f];
            }
            Console.WriteLine("el elemento mayor es: " + max);
            Console.WriteLine("el elemento menor es: " + min);

            for (int f = 0; f < alturas.Length; f++)
            {
                if (alturas[f] == max)
                {
                    Console.WriteLine("{0} tiene la altura maxima de: {1} ", alumnos[f], max);
                }
                if (alturas[f] == min)
                {
                    Console.WriteLine("{0} tiene la altura minima de: {1} ", alumnos[f], min);
                }
            }
        }

        static void Main(string[] args)
        {
            PruebaVector2 pv2 = new PruebaVector2();
            pv2.Cargar();
            pv2.CalcularPromedio();
            pv2.MayoresMenores();
            pv2.may();
            Console.ReadKey();
        }
    }
}
