﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena;
            char letra;
            Console.Write("Escriba una palabra: ");
            cadena = Console.ReadLine();
            Console.Write("Escriba la letra a buscar: ");
            letra = char.Parse(Console.ReadLine());
            //llama la funcion contar y envia los parametros cadena y letra
            //a los valores cadena1 y letra2
            Console.WriteLine("{0} esta {1} veces en {2}", letra, Contar(cadena, letra), cadena);
            Console.ReadKey(); 
        }

        private static int Contar(string cadena1, char letra2)
        {
            int n = 0;
            for (int i = 0; i < cadena1.Length; i++)
                //si el valor de la posicion i de cadena1 es = a letra2
                //aumenta el contador en 1
                if (cadena1[i] == letra2)
                    n++;
            return n;
        } 
    }
}
