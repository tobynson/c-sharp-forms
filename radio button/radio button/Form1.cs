﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private string categoria, dinero;

        public Form1()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            //label1.Text = "Categoria A";
            categoria = "Categoria A";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            //label1.Text = "Categoria B";
            categoria = "Categoria B";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            //label1.Text = "Categoria C";
            categoria = "Categoria C";
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            dinero = "100 soles";
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            dinero = "200 soles";
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            dinero = "300 soles";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = " la categoria seleccionada es " + categoria + " el dinero seleccionado es " + dinero;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
