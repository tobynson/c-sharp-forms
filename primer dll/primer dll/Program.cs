﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//llamada al dll creado
using DLL;

namespace aplicacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("ingrese el primer numero de la suma: ");
            double a1 = double.Parse(Console.ReadLine());
            Console.Write("ingrese el segundo numero de la suma: ");
            double b2 = double.Parse(Console.ReadLine());
            //instancio el objeto obj_dll de la clase class1 de la libreria DLL
            Class1 obj_dll = new Class1();
            //llamo al metodo suma del objeto obj_dll y el resultado le asigno a 
            //la variable resultado del tipo doble
            double resultado = obj_dll.suma(a1,b2);
            Console.WriteLine("la suma es: " + resultado);
            Console.ReadLine();

        }
    }
}
