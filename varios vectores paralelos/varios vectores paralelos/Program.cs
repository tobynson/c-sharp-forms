﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PruebaVector16
{
    class PruebaVector16
    {
        //campo de datos
        private string[] nombres;
        private int[] notas;
        public int[] edad;
        private float[] tallas;
        //cantidad de datos a procesar
        int n;

        public void Cargar()
        {
            Console.Write("cuantos datos desea procesar:");
            n = int.Parse(Console.ReadLine());
            //inicializa los vectores con la cantidad n
            nombres = new string[n];
            notas = new int[n];
            edad = new int[n];
            tallas = new float[n];
            Console.WriteLine("Carga de nombres y notas");
            for (int f = 0; f < nombres.Length; f++)
            {
                Console.Write("Ingese el nombre del alumno:");
                nombres[f] = Console.ReadLine();
                Console.Write("Ingrese la nota del alumno:");
                string linea;
                linea = Console.ReadLine();
                notas[f] = int.Parse(linea);
                Console.Write("Ingrese la edad del alumno:");
                edad[f] = int.Parse(Console.ReadLine());
                Console.Write("Ingese talla del alumno:");
                tallas[f] = float.Parse(Console.ReadLine());

            }
            Console.Clear();
        }



        public void Imprimir()
        {
            Console.WriteLine("Nombres  notas edad y tallas");
            for (int f = 0; f < notas.Length; f++)
            {
                Console.WriteLine(nombres[f] + " ---- " + notas[f] + " --- " + edad[f] + "--- " + tallas[f]);
            }

        }

        public void mayoresedad()
        {
            Console.WriteLine("mayores de edad");
            for (int f = 0; f < notas.Length; f++)
            {
                if (edad[f] > 18)
                {
                    Console.WriteLine(nombres[f] + " "+  edad[f]);
                }
                else
                {
                    Console.WriteLine("ningun mayor de edad");
                }
            }

        }

        public void mayorestalla()
        {
            Console.WriteLine("mayores talla");
            for (int f = 0; f < notas.Length; f++)
            {
                //float x =1,50;
                if (tallas[f] > 1.50)
                {
                    Console.WriteLine(tallas[f]);
                }
                else
                {
                    Console.WriteLine("tallas menores a 1.50");
                }
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            PruebaVector16 pv = new PruebaVector16();
            pv.Cargar();


            pv.Imprimir();
            pv.mayoresedad();
            pv.mayorestalla();
        }
    }
}
